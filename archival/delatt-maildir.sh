#!/usr/bin/env bash

SRCDIR=${1}
TRGDIR=${2}

BINDIR="${HOME}/.local/bin"


mkdir -p "${TRGDIR}"

cd "${SRCDIR}"

find -type d -exec mkdir -p "${TRGDIR}"/{} \;

for f in $(find -type f); do
    "${BINDIR}/pydelatt.py" -o -t "${f}" > "${TRGDIR}/${f}"
done

