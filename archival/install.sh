#!/usr/bin/env bash

DIR="$(cd -P "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )" && pwd)"

BINDIR="${HOME}/.local/bin"


git clone git://gist.github.com/6572826.git "${DIR}/pydelatt"
chmod +x "${DIR}/pydelatt/pydelatt.py"

ln -sfn "${DIR}"/{pydelatt/pydelatt.py,delatt-maildir.sh,archive-comm.sh}   "${BINDIR}/"

