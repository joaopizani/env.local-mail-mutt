#!/usr/bin/env bash

DIR="$(cd -P "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )" && pwd)"

BINDIR="${HOME}/.local/bin"


mkdir -p "${HOME}/comm/mutt"
mkdir -p "${DIR}/mutt/temp" "${DIR}/mutt/cache/"{headers,bodies}
mkdir -p "${HOME}/Downloads/mutt-attachments"

ln -fsn "${HOME}/comm/mutt"  "${HOME}/.mail"
ln -fsn "${DIR}/mutt"        "${HOME}/.mutt"

ln -fsn "${DIR}/archival/mutt-open.sh"  "${BINDIR}/"

ln -fsnr "${DIR}/mutt/themes/inkpot-256.theme"  "${DIR}/mutt/themes/current-theme.rc"


